from django.db import models
from datetime import  datetime
# Create your models here.


class TransportType(models.Model):
    """
    Transport Type
    """
    name = models.CharField(max_length=255, db_index=True, verbose_name="Transport Type", help_text="Transport Type",unique=True)
    desc = models.CharField(max_length=255, db_index=True,null=True, blank=True, verbose_name="Transport Type Description", help_text="Transport Type Description")
    createtime = models.DateTimeField(default=datetime.now, verbose_name="create time")
    modify_time = models.DateTimeField(null=True, blank=True)
    is_active = models.BooleanField(default=True, db_index=True, verbose_name="is active")
    deactivetime = models.DateTimeField(null=True, blank=True, verbose_name="deactive time")

    class Meta:
        verbose_name = u"TransportType"
        verbose_name_plural = verbose_name

    def __str__(self):
        return str(self.name)