from django.shortcuts import render

# Create your views here.
from rest_framework import serializers
from .models import TransportType
from rest_framework import mixins
from rest_framework import viewsets

class TransportTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = TransportType
        fields = "__all__"

class TransportTypeViewSet(mixins.CreateModelMixin,mixins.RetrieveModelMixin,mixins.UpdateModelMixin,mixins.ListModelMixin,viewsets.GenericViewSet):
    # throttle_classes = (UserRateThrottle, )
    queryset = TransportType.objects.all()
    # permission_classes = (IsAuthenticated,)
    # authentication_classes = (JSONWebTokenAuthentication, SessionAuthentication)
    serializer_class = TransportTypeSerializer
    # filter_backends = [DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter]
    # filter_class = ProductionProcessCatogoryFilter
    # search_fields = ('name', )
    # ordering_fields = ('createtime',)